<?php include("db.php") ?>
<?php include("header.php") ?>

        <div class="container p-4">
           <div class="row">
              <div class="col-md-4">
                
                <?php if(isset($_SESSION['message'])) { ?>
                    <div class="alert alert-<?= $_SESSION['message_type']; ?> alert-dismissible fade show" role="alert">
                    <?=  $_SESSION['message'] ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                <?php session_unset(); } ?>
                
                 <div class="card card-body">
                     <form action="save_task.php" method="post">
                         <div class="form-group">
                             <input type="text" name="cedula" class="form-control" placeholder=" Ingrese cedula " autofocus>
                         </div>
                         
                         <div class="form-group">
                             <input type="text" name="nombre" class="form-control" placeholder=" Ingrese nombre " autofocus>
                         </div>
                         
                         <div class="form-group">
                             <input type="number" name="edad" class="form-control" placeholder=" Ingrese edad " autofocus>
                         </div>
                         
                         <div class="form-group">
                             <input type="text" name="direccion" class="form-control" placeholder=" Ingrese direccion " autofocus>
                         </div>
                         
                         <input type="submit" class="btn btn-success btn-block" name="guardar" value="Guardar">
                     </form>
                 </div>                  
              </div>
            
            <div class="col-md-8">
                <table class="table table-bordered">
                   <thead>
                       <tr>
                           <th>Cedula</th>
                           <th>Nombre</th>
                           <th>Edad</th>
                           <th>Direccion</th>
                           <th>Creacion</th>
                           <th>Opciones</th>
                       </tr>
                   </thead>
                   <tbody>
                       <?php
                       
                       $query = "SELECT * FROM task";
                       $result_tasks = mysqli_query($conn, $query);
                       
                       while($row  = mysqli_fetch_array($result_tasks)) { ?>
                           <tr>
                               <td> <?php echo $row['cedula'] ?> </td>
                               <td> <?php echo $row['nombre'] ?> </td>
                               <td> <?php echo $row['edad'] ?> </td>
                               <td> <?php echo $row['direccion'] ?> </td>
                               <td> <?php echo $row['created_at'] ?> </td>
                               <td>
                                   <a href="edit.php?id=<?php echo $row['id'] ?>" class="btn btn-secondary">
                                       <i class="fas fa-marker"></i>
                                   </a>
                                   
                                   <a href="delete_task.php?id=<?php echo $row['id'] ?>" class="btn btn-danger">
                                      <i class="far fa-trash-alt"></i> 
                                   </a>
                               </td>
                           </tr>
                       
                       <?php } ?>
                   </tbody>                    
                </table>
            </div>    
                                            
           </div>            
        </div>
                        
<?php include("footer.php") ?>
