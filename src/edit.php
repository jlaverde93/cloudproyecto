<?php
include("db.php");
$title = '';
$description= '';

if  (isset($_GET['id'])) {
    $id = $_GET['id'];
    $query = "SELECT * FROM task WHERE id=$id";
    $result = mysqli_query($conn, $query);
    
    if (mysqli_num_rows($result) == 1) {
        $row = mysqli_fetch_array($result);
        $cedula = $row['cedula'];
        $nombre = $row['nombre'];
        $edad = $row['edad'];
        $direccion = $row['direccion'];    
    }
}

if (isset($_POST['update'])) {
    $id = $_GET['id'];
    $cedula= $_POST['cedula'];
    $nombre = $_POST['nombre'];
    $edad = $_POST['edad'];
    $direccion = $_POST['direccion'];

    $query = "UPDATE task set cedula = '$cedula', nombre = '$nombre', edad = '$edad', direccion = '$direccion' WHERE id=$id";
    mysqli_query($conn, $query);
    $_SESSION['message'] = 'Task Updated Successfully';
    $_SESSION['message_type'] = 'warning';
    header('Location: index.php');
}

?>

<?php include('header.php'); ?>
 <div class="container p-4">
   <div class="row">
     <div class="col-md-4 mx-auto">
      <div class="card card-body">
       <form action="edit.php?id=<?php echo $_GET['id']; ?>" method="POST">
         <div class="form-group">
             <input name="cedula" type="text" class="form-control" value="<?php echo $cedula; ?>" placeholder="Update C.I.">
         </div>
         
         <div class="form-group">
             <input name="nombre" type="text" class="form-control" value="<?php echo $nombre; ?>" placeholder="Update nombre">
        </div>
        
        <div class="form-group">
             <input name="edad" type="number" class="form-control" value="<?php echo $edad; ?>" placeholder="Update edad">
        </div>
        
        <div class="form-group">
             <input name="direccion" type="text" class="form-control" value="<?php echo $direccion; ?>" placeholder="Update direccion">
        </div>
         
         <button class="btn-success" name="update">
          Update
          </button>
        </form>
    </div>
    </div>
    </div>
</div>
<?php include('footer.php'); ?>
