CREATE TABLE task 
( `id` INT NOT NULL AUTO_INCREMENT , 
`cedula` VARCHAR(10) NOT NULL , 
`nombre` VARCHAR(20) NOT NULL , 
`edad` INT(2) NOT NULL , 
`direccion` VARCHAR(20) NOT NULL , 
`created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP , 
PRIMARY KEY (`id`));

INSERT INTO task (`cedula`, `nombre`, `edad`, `direccion`) VALUES
('0503884736', 'Alexandra', 26, 'Valencia'),
('1204607293', 'Liliana', 48, 'Granada'),
('1204607293', 'Alex', 26, 'Ec');
