# CRUD BASICO 

_Proyecto final para el modulo CloudComputing con la ayuda de contenedores, a continuacion se detalla contenido de carpetas:_

* [mysql](https://gitlab.com/jlaverde93/cloudcomputing/tree/master/mysql) - Script de la base de datos llamada crud con una tabla task, la cual contiene los campos id,cedula,nombre,edad, dirección.
* [src](https://gitlab.com/jlaverde93/cloudcomputing/tree/master/src) - Archivos básicos y sencillo para la BD


## Construido con 🛠️

* Php 7
* Mysql 8


## Comenzando 🚀

### Pre-requisitos 📋

_Se trabaja con una maquina virtual en donde ya debe estar descargado Docker_

```
sudo apt install docker.io
```
_Las versiones de PHP7 y MySQL 8, con los siguientes comandos respectivamente:_

```
sudo docker pull php
sudo docker pull mysql
```
Mientras que en la máquina local se ha instalado MySQL Workbench.
### Instalación 🔧

_Desde el terminal de ejecutar el siguiente comando para clonar este repositorio en la máquina virtual_
```
git clone https://gitlab.com/jlaverde93/cloudproyecto.git
```
_Seguido para construir la estructura previamente ya configurada en este proyecto el comando:_

```
sudo docker-compose build
```
_Para levantar o ejecutar el proyecto_

```
sudo docker-compose up
```

## Ejecutando  ⚙️

_Abre Workbench crea una nueva conexion con la siguiente información:_
* Hostname : 192.168.100.250 
*_En mi caso esa es la IP que estoy usando_*
* Port: 8081
* Password: 123456
 
_Automaticamente se creará lo realizado en el script del repositorio_
_En algun navegador de tu preferencia en la barra de direcciones digita:_
```
192.168.100.250:8000

```
_El proyecto se cargará con los datos que ya fueron guardados previamente_

## Versionado 📌

Esta es la versión 2.0 del proyecto

## Autores ✒️

* **Jeannette Laverde** 
